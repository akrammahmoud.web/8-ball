# Una partita a Palla 8

## Prologo
Mario e Luigi si sfidano a una partita di Palla 8; Mario deve imbucare le palle piene (spot) e Luigi quelle spezzate (stripe).
Vince chi imbuca per primo tutte le proprie palle e per finire la palla 8.

## Dati
Nella cartella data ci sono 3 JSON:
- config.json: l'elenco delle palle ordinate per numero da 1 a 15, dei giocatori e delle buche.
- match.json: l'elenco non oridinato per tempo di dove e da chi le palle sono state imbucate.
- timestamps.json: un dizionario che ha come chiave il timestamp in cui la palla è stata imbucata e come valore l'id della palla.

## Istruzioni
- Recupera i dati emulando una chiamata http (no import o require).
- Individua chi ha vinto.
- Per il vincitore, crea una tabella con nell'asse delle x l'ordine di imbucamento e nell'asse delle y le buche. Popola le celle della tabella con il numero della palla colorando il numero o la cella del colore della palla stessa come da esempio:
- Crea un branch con il tuo nome e committa il progetto

|    | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|:--:|:-:|:-:|:-:|:-:|:-:|:-:|---|:-:|
| topRight | <font color="blue">2</font> |   |   |   |   |   |   |   |
| centerRight |   |   |   |   |   | <font color="green">6</font> |   | <font color="black">8</font> |
| bottomRight |   | <font color="purple">4</font> |   |   |   |   |   |   |
| topLeft |   |   |   |   |   |   | <font color="orange">5</font> |   |
| centerLeft |   |   | <font color="gold">1</font> | <font color="red">3</font> |   |   |   |   |
| bottomLeft |   |   |   |   | <font color="brown">7</font> |   |   |   |

- Crea un branch e committa.

Sentiti libero di sviluppare il progetto come preferisci: Angular, React, Vanilla Js, Vue.js, etc...

## Buon lavoro!
Per qualsiasi problema contattami per email!

